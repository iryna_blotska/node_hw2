const mongoose = require('mongoose');

const User = mongoose.model('User', { 
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { User }