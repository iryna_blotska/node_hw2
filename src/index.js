require('dotenv').config();
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const app = express();

const {userRouter} = require('./controllers/userController');
const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));
app.use(cookieParser());
app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/notes', notesRouter);
app.use('/api/users', userRouter);

const start = async () => {
    try {
        await mongoose.connect(process.env.DB_PATH, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        app.listen(process.env.DB_PORT);
    } catch (err) {
        console.error('Error on start', err.message);
    }
}

start();