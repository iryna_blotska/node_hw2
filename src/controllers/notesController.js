const express = require('express');
const router = express.Router();
const {countNotes, getNotesByUserId, addNoteToUser, getNoteById, deleteNoteById, updateNoteById} = require('../services/notesService')

let offset = 0;
const limit = 5;

router.get('/', async (req, res) => {
    try {
        const {userId} = req.user;
        const notes = await getNotesByUserId(userId, offset, limit);
        const count = await countNotes(userId);
        res.json({
            offset: offset,
            limit: limit,
            count: count,
            notes: notes
        });
        // if(offset < count - offset){
        //     return offset = offset + limit;
        // }
    } catch (err){
        res.status(500).json({ message: err.message });
    }
    
});

router.post('/', async (req, res) => {
    try {
        const {userId} = req.user;
        const note = await addNoteToUser(userId, req.body);
        res.json({message: "Success"});
    } catch (err){
        res.status(500).json({ message: err.message });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const { userId } = req.user;
        const { id } = req.params;
        const note = await getNoteById(id, userId);

        if (!note) {
            res.status(404).json({ message: 'Note not found!' });
        }

        res.json({note});
    } catch (err){
        res.status(500).json({ message: err.message });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const { userId } = req.user;
        const { id } = req.params;
        const note = await deleteNoteById(id, userId);

        if (!note) {
            res.status(404).json({ message: 'Note not found!' });
        }

        res.json({message: "Success"});
    } catch (err){
        res.status(500).json({ message: err.message });
    }
});
router.patch('/:id', async (req, res) => {
    try {
        const { userId } = req.user;
        const { id } = req.params;
        
        const note = await updateNoteById(id, userId);

        if (!note) {
            res.status(404).json({ message: 'Note not found!' });
        }

        res.json({message: "Success"});
    } catch (err){
        res.status(500).json({ message: err.message });
    }
});

// updateNoteById


module.exports = {
    notesRouter: router
}