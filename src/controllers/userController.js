const express = require('express');
const router = express.Router();
const {getUserInfo} = require('../services/userService');


router.get('/me', async (req, res) => {
    try {
        const {userId} = req.user;
        const user = await getUserInfo(userId);
        res.json({user});
    } catch (err){
        res.status(500).json({ message: err.message });
    }
    
});

module.exports = {
    userRouter: router
}