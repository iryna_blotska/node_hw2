const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
    // const token = req.cookies.access_token;

    // if(!token){
    //     return res.status(401).json({message: "Please, include token"});
    // }
    const {
        authorization
    } = req.headers;

    if (!authorization) {
        return res.status(401).json({message: 'Please, provide "authorization" header'});
    }

    const [, token] = authorization.split(' ');

    if (!token) {
        return res.status(401).json({message: 'Please, include token to request'});
    }
    try {
        const tokenPayload = jwt.verify(token, process.env.DB_SECRET);
        req.user = {
            userId: tokenPayload._id,
            userName: tokenPayload.username
        }
        next();
    } catch (err){
        res.status(401).json({message: "Invalid token"});
    }
}

module.exports = {
    authMiddleware
}