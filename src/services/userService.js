const { User } = require('../models/userModel');

const getUserInfo = async(userId) => {
    const user = await User.find({_id: userId});
    console.log(user)
    return user;
}

module.exports = {
    getUserInfo
}