const { User } = require('../models/userModel');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken'); 

const register = async ({username, password}) => {
    const user = new User({
        username,
        password: await bcrypt.hash(password, 10)
    });

    await user.save();
}

const login = async ({username, password}) => {
    const user = await User.findOne({username});

    if (!user) {
        throw new Error('Invalid email or password');
    }

    // try {
    //     await bcrypt.compare(password, user.password);
    // } catch (err){
    //     throw new Error('Invalid email or password');
    // }

    const token = jwt.sign({
        _id: user._id,
        username: user.username
    }, 'secret');
    return token;
}

module.exports = {
    register, login
}