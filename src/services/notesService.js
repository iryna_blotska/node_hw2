const {Note} = require('../models/noteModel');

const countNotes = async (userId) => {
    const count = await Note.find({userId}).count();
    return count;
}
const getNotesByUserId = async (userId, offset, limit) => {
    const notes = await Note.find({userId}).skip(offset).limit(limit);
    return notes;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save();
}

const getNoteById = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
}
const deleteNoteById = async (noteId, userId) => {
    const note = await Note.deleteOne({_id: noteId, userId});
    return note;
}
const updateNoteById = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    const complitedNotenote = await Note.where({ _id: noteId, userId }).updateOne({ $set: { completed: !note.completed }});
    return complitedNotenote;
    
}

module.exports = {
    countNotes,
    getNotesByUserId,
    addNoteToUser,
    getNoteById,
    deleteNoteById,
    updateNoteById
}