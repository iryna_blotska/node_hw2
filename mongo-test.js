const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://testuser99:B6KrpxjrZD3B5AG@cluster0.ogaqw.mongodb.net/hw2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Notes = mongoose.model('Note', { 
    text: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    complete: Boolean,
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

(async () => {
    try {
        // const note = new Notes({ 
        //     text: 'Note 2',
        //     userId: 'Peter'
        // });
        // await note.save();
        
        //remove note
        //await Notes.findByIdAndRemove('60f18edf5d086ad19f1b1361');
        
        //notes list
        const notes = await Notes.find({});
        console.log(notes)
        
        //.then(() => console.log('meow'));
        
    } catch (err){
        console.error('Error', err)
    }
    
})();